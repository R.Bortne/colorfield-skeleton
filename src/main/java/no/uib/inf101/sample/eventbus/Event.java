package no.uib.inf101.sample.eventbus;

/**
 * An event that can be posted on the event bus. All events must
 * implement this interface in order to be posted on the event bus.
 */
public interface Event { }
